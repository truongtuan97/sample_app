require 'test_helper'

class UserLayoutTest < ActionDispatch::IntegrationTest
  test "signup title" do
  	get signup_path
  	assert_select 'title', full_title("Sign up")
  end
end
